package _05dice;

import javax.swing.*;
import java.awt.*;

public class FlagDrawer extends JComponent {
    private int offset=0;
    public void paintComponent(Graphics g){
        drawFlag(g, Color.BLACK, Color.RED, Color.YELLOW);
        drawFlag(g, Color.RED, Color.WHITE, Color.GREEN);
    }
    public void drawFlag(Graphics g, Color c1, Color c2, Color c3){
        g.setColor(c1);
        g.fillRect(80, 30-offset, 150, 40);

        g.setColor(c2);
        g.fillRect(80, 70-offset, 150, 40);

        g.setColor(c3);
        g.fillRect(80, 110-offset, 150, 40);

        offset=offset-200;
    }
}
