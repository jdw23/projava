package _05dice;

import javax.swing.*;

public class FlagsViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(300, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new FlagDrawer();
        frame.add(component);

        frame.setVisible(true);
    }
}
