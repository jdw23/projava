package _05dice.pig;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Pig extends JFrame {
    private JLabel yourScore;
    private JLabel cpuScore;
    private JLabel lastCPURoll;
    private JLabel yourLastRoll;
    private JButton roll;
    private JButton endTurn;


    public Pig() {
        this.buildComponents();
        this.buildPanel();
        super.setSize(400, 150);
        super.setVisible(true);
    }

    private void buildComponents() {
        this.yourScore = new JLabel("0");
        this.cpuScore = new JLabel("0");
        this.lastCPURoll = new JLabel("ROLL");
        this.yourLastRoll = new JLabel("ROLL");
        this.buildButtons();
    }
    //could make this AI smarter, but thought I'd make the game easy for the player :)
    private void cpuTurn() {
        Random rand = new Random();
        ArrayList<Integer> cPURoll = new ArrayList<Integer>();
        int roll = rand.nextInt(5) + 1;
        for (int i = 0; i < roll; i++) {
            cPURoll.add(new Integer(rand.nextInt(5) + 1));
        }

        if (cPURoll.contains(new Integer(1))) {
            lastCPURoll.setText("BUST");
        } else {
            int total = 0;
            String set = "";
            for (Integer i : cPURoll) {
                total += i;
                set = set+Integer.toString(i)+" ";
            }
            total += Integer.valueOf(cpuScore.getText());
            cpuScore.setText(Integer.toString(total));
            lastCPURoll.setText(set);
            if (total >= 100) {
                JOptionPane.showMessageDialog(null, "YOU LOSE", "YOU LOST", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    private void buildButtons() {
        this.roll = new JButton("Roll");
        this.endTurn = new JButton("End Turn");
        //Roll operation
        this.roll.addActionListener(new ActionListener(){
            ArrayList<Integer> currentRoll = new ArrayList<Integer>();

            public void actionPerformed(ActionEvent e) {
                Random rand = new Random();
                Integer num = new Integer(rand.nextInt(6) + 1);
                //case if you go bust
                //give youself bust and no points
                if (num.compareTo(new Integer(1)) == 0) {
                    currentRoll = new ArrayList<Integer>();
                    yourLastRoll.setText("BUST");
                    cpuTurn();
                }
                //if you dont bust
                else {
                    currentRoll.add(num);
                    if (yourLastRoll.getText().equals("BUST") || yourLastRoll.getText().equals("ROLL")) {
                        yourLastRoll.setText(currentRoll.get(0).toString());
                    } else {
                        yourLastRoll.setText(yourLastRoll.getText() + " " + currentRoll.get(currentRoll.size() - 1));
                    }
                }
            }
        });
        this.endTurn.addActionListener(new ActionListener() {
        //End turn operation
            public void actionPerformed(ActionEvent e) {
                int total = Integer.parseInt(yourScore.getText());
                if (yourLastRoll.getText().equals("BUST") || yourLastRoll.getText().equals("ROLL")){
                    cpuTurn();
                }
                else {
                    String[] lastRoll = yourLastRoll.getText().split(" ");
                    for (String roll : lastRoll){
                        total+= Integer.parseInt(roll);
                    }
                    if (total >=100){
                        JOptionPane.showMessageDialog(null, "YOU WIN", "YOU WON", JOptionPane.INFORMATION_MESSAGE);
                    }
                    cpuTurn();
                    yourLastRoll.setText("ROLL");
                    yourScore.setText(Integer.toString(total));
                }
            }

    });
}

    private void buildPanel() {
        JPanel panel = new JPanel(new GridLayout(3,3));
        panel.add(new JLabel ("P1 score,last roll"));
        panel.add(this.yourScore);
        panel.add(this.yourLastRoll);
        panel.add(new JLabel ("CPU score,last roll"));
        panel.add(this.cpuScore);
        panel.add(this.lastCPURoll);

        panel.add(this.roll);
        panel.add(this.endTurn);
        super.add(panel);
    }
}
