package _05dice.P10_19;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BillFrame extends JFrame {
    private JButton burger;
    private JButton fries;
    private JButton coke;
    private JButton lemonade;
    private JButton nuggets;
    private JButton tacos;
    private JButton animalStyle;
    private JButton beer;
    private JButton tenders;
    private JButton tots;
    private JButton getBill;
    private JLabel current;

    public BillFrame(){
        this.buildComponents();
        this.buildPanel();
        super.setSize(400,150);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setVisible(true);
    }

    private void buildComponents(){
        this.current=new JLabel("0.00");
        this.buildButtons();
    }

    private void buildButtons(){
        burger =    new JButton("Burger");
        fries  =    new JButton("Fries");
        coke   =    new JButton("Coke");
        lemonade=   new JButton("Lemonade");
        nuggets =   new JButton("Chicken Nuggets");
        tacos  =    new JButton("Tacos");
        animalStyle=new JButton("Animal Style Burger");
        beer   =    new JButton("Beer");
        tenders =   new JButton("Chicken Tenders");
        tots   =    new JButton("Tater Tots");
        getBill = new JButton("Get the bill");

        class AddItemListener implements ActionListener {
            private double itemPrice;

            public AddItemListener(double itemPrice){
                this.itemPrice = itemPrice;
            }

            public void actionPerformed(ActionEvent e){
                double currentBillSum = Double.valueOf(current.getText())+this.itemPrice;
                current.setText(String.format("%.2f",currentBillSum));
            }
        }

        burger.addActionListener(new AddItemListener(3));
        fries.addActionListener(new AddItemListener(1));
        coke.addActionListener(new AddItemListener(1));
        lemonade.addActionListener(new AddItemListener(2));
        nuggets.addActionListener(new AddItemListener(2.50));
        tacos.addActionListener(new AddItemListener(5));
        animalStyle.addActionListener(new AddItemListener(3.50));
        beer.addActionListener(new AddItemListener(5));
        tenders.addActionListener(new AddItemListener(3));
        tots.addActionListener(new AddItemListener(1));

        getBill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, String.format("Your bill is: %s, please tip %.2f",
                        current.getText(), Double.parseDouble(current.getText()) * .2,
                        "PURCHASE FINALIZED!", JOptionPane.INFORMATION_MESSAGE));
            }
        });
    }

    private void buildPanel(){
        JPanel panel = new JPanel();
        panel.add(this.current);
        panel.add(this.getBill);
        panel.add(this.burger);
        panel.add(this.animalStyle);
        panel.add(this.fries);
        panel.add(this.tots);
        panel.add(this.tacos);
        panel.add(this.nuggets);
        panel.add(this.tenders);
        panel.add(this.coke);
        panel.add(this.lemonade);
        panel.add(this.beer);
        super.add(panel);
    }

    public static void main(String[] args) {
        JFrame frame = new BillFrame();
    }
}
