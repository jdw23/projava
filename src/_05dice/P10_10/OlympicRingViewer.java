package _05dice.P10_10;

import javax.swing.*;

public class OlympicRingViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        final int FRAME_WIDTH = 300;
        final int FRAME_HEIGHT = 230;

        frame.setSize(300, 230);
        frame.setTitle("OlympicRingViewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        RingDrawer component = new RingDrawer();
        frame.add(component);

        frame.setVisible(true);
    }
}
