package _05dice.P10_10;

import javax.swing.*;
import java.awt.*;

public class RingDrawer extends JComponent {
    public void paintComponent(Graphics g){
        drawRing(g,Color.YELLOW,1,0);
        drawRing(g,Color.BLACK,21,20);
        drawRing(g,Color.GREEN,41,0);
        drawRing(g,Color.RED,61,20);
        drawRing(g,Color.BLUE,81,0);
    }
    public void drawRing(Graphics g, Color c, int x, int y){
        g.drawOval(x,y,40,40);
        g.setColor(c);
    }
}
