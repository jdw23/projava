package _05dice.P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
//https://stackoverflow.com/questions/10244823/draw-a-circle-using-2-mouse-clicks
public class ClickCircle {
    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame("Clicker");
        frame.add(new JComponent() {
            Point p1, p2;
            {
                addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (p1== null || p2 != null){
                            p1 = e.getPoint();
                            p2 = null;
                        }
                        else {
                            p2 = e.getPoint();
                        }
                        repaint();
                    }
                });
                setPreferredSize(new Dimension(600,500));
            }

            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                if (p1 != null && p2 != null){
                    int r = (int) Math.round(p1.distance((p2)));
                    g.drawOval(p1.x - r, p1.y - r, 2*r, 2*r);
                }
            }
        });
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
