package _04interfaces.E9_8;

public abstract class BankAccount {
    private double balance;
    public void deposit(double amount){
        balance=balance+amount;
    }
    public abstract void withdraw(double amount);
    public double getBalance(){
        return balance;
    }
    public abstract void process();
}
