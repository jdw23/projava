package _04interfaces.E9_8;

public class BasicAccount extends BankAccount{
    private double balance;
    public BasicAccount(double b){
        balance = b;
    }

    public void withdraw(double amount) {
        if (amount > balance) {
            System.out.println("Don't overdraw this account!");
            return;
        }
        balance=balance-amount;
    }

    public void process(){
        return;
    }
}
