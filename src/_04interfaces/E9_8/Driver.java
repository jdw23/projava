package _04interfaces.E9_8;

public class Driver {
    public static void main(String[] args) {
        BasicAccount mine = new BasicAccount(100);
        for (int i = 0; i<100;i++){
            mine.withdraw(1.01);
        }
        System.out.println(mine.getBalance());
    }
}
