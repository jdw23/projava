package _04interfaces.E9_17;

public class Driver {
    public static void main(String[] args) {
        SodaCan[] cans = new SodaCan[10];
        for (int i=0; i<10;i++){
            cans[i]= new SodaCan(2,4);
        }
        double sum=0;
        for (SodaCan can : cans){
            sum=sum+can.getMeasure();
        }
        double avg = sum / cans.length;
        System.out.println(avg);
    }
}
