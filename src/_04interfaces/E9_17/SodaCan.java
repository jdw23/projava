package _04interfaces.E9_17;

public class SodaCan implements Measurable {
    private double radius;
    private double height;
    public SodaCan(double r, double h) {
        radius = r;
        height = h;
    }
    @Override
    //A = 2 πrh + 2 πr2
    public double getMeasure() {
        return 2*Math.PI*radius*height+2*Math.PI*Math.pow(radius,2);
    }
}
