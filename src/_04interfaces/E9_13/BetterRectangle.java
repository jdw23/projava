package _04interfaces.E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int w,int h){
        super.setLocation(x,y);
        super.setSize(w,h);
    }
    public double getPerimeter(){
        return 2*super.getHeight()+2*super.getWidth();
    }
    public double getArea(){
        return super.getHeight()*super.getWidth();
    }
}
