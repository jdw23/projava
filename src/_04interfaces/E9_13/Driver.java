package _04interfaces.E9_13;

public class Driver {
    public static void main(String[] args) {
        BetterRectangle rect;
        for (int x = 1;x<3;x++){
            for (int y=1;y<3;y++){
                rect = new BetterRectangle(x,y,x,y);
                System.out.println("Area: "+rect.getArea());
                System.out.println("Perimeter: "+rect.getPerimeter());
            }
        }
    }
}
