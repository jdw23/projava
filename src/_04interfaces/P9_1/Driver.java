package _04interfaces.P9_1;

public class Driver {
    public static void main(String[] args) {
        Clock now = new Clock(java.time.LocalTime.now());
        WorldClock nowNY = new WorldClock(java.time.LocalTime.now(),5);
        System.out.println(now);
        System.out.println(nowNY);
    }

}
