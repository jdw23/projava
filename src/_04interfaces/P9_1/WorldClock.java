package _04interfaces.P9_1;

import java.time.LocalTime;

public class WorldClock extends Clock {
    public WorldClock(LocalTime t, int timeZone){
        super(t.plusHours(timeZone));
    }
}
