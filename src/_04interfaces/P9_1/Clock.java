package _04interfaces.P9_1;

import java.time.LocalTime;

public class Clock {
    LocalTime time;
    public Clock(LocalTime t){
        time = t;
    }

    public String getTime() {
        return Integer.toString(time.getHour())
                +":"+Integer.toString(time.getMinute());
    }
    public int getHours(){
        return time.getHour();
    }
    public int getMinutes(){
        return time.getMinute();
    }

    @Override
    public String toString() {
        return "Clock{" +
                "time=" + time +
                '}';
    }
}
