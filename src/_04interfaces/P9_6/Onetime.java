package _04interfaces.P9_6;

public class Onetime extends Appointment{
    public Onetime(String d, int y, int m, int da){
        super( d, y, m, da);
    }
    @Override
    public boolean occursOn(int y, int m, int d) {
        return y==this.getYear() && m==this.getMonth()
                && d==this.getDay();
    }

}
