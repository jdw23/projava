package _04interfaces.P9_6;

public abstract class Appointment {
    String desc;
    int year;
    int month;
    int day;
    public Appointment(String d, int y, int m, int da){
        desc=d;
        year=y;
        month=m;
        day=da;
    }
    public abstract boolean occursOn(int y, int m, int d);

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "desc='" + desc + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }
}
