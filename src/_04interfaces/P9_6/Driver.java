package _04interfaces.P9_6;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Appointment[] appts = new Appointment[365];
        for(int i=0;i< appts.length;i++){
            Appointment appt;
            if (i%3==0){
                appts[i]=new Daily("shots",i*10,i%12,i%30);
            }
            if (i%3==1){
                appts[i]=new Monthly("dentist",i*10,i%12,i%30);
            }
            if (i%3==2){
                appts[i]=new Daily("check up",i*10,i%12,i%30);
            }

        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 3 seperate ints corresponding to day month year");
        System.out.println("Example: 12 14 1999");
        int m = scan.nextInt();
        int d = scan.nextInt();
        int y = scan.nextInt();
        for (Appointment appt : appts){
            if (appt.occursOn(y,m,d)){
                System.out.println(appt);
            }
        }
    }
}
