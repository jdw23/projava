package _04interfaces.P9_6;

public class Daily extends Appointment{
    public Daily(String d, int y, int m, int da){
        super( d, y, m, da);
    }
    @Override
    //It's somewhat unclear if this is supposed to be
    //Every day forever or just times after the first
    //From the sounds of the month example in the book,
    //It should just be forever
    public boolean occursOn(int y, int m, int d) {
        return true;
    }
}
