package _04interfaces.P9_6;

public class Monthly extends Appointment{
    public Monthly(String d, int y, int m, int da){
        super( d, y, m, da);
    }
    @Override
    public boolean occursOn(int y, int m, int d) {
        return m==this.getMonth();
    }
}
