package _08final.mvc.controller;

import _08final.mvc.model.*;
import _08final.mvc.view.GamePanel;
import _08final.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import static java.awt.Event.DOWN;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

    // ===============================================
    // FIELDS
    // ===============================================

    public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
    private GamePanel gmpPanel;
    public static Random R = new Random();
    public final static int ANI_DELAY = 45; // milliseconds between screen
    // updates (animation)
    private Thread thrAnim;
    private int nLevel = 1;
    private int nTick = 0;

    private boolean bMuted = true;


    private final int PAUSE = 80, // p key
            QUIT = 81, // q key
            LEFT = 37, // rotate left; left arrow
            RIGHT = 39, // rotate right; right arrow
            UP = 38, // thrust; up arrow
            DOWN = 40,
            START = 83, // s key
            FIRE = 32, // space key
            MUTE = 77, // m-key mute


    // for possible future use
    // HYPER = 68, 					// d key
    // SHIELD = 65, 				// a key arrow
    // NUM_ENTER = 10, 				// hyp
    SPECIAL = 70,                    // fire special weapon;  F key
    CHARGESPECIAL = 10;

    private Clip clpThrust;
    private Clip clpMusicBackground;
    private Clip clpLaser;

    private static final int SPAWN_NEW_SHIP_FLOATER = 1200;


    // ===============================================
    // ==CONSTRUCTOR
    // ===============================================

    public Game() {

        gmpPanel = new GamePanel(DIM);
        gmpPanel.addKeyListener(this);
        clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
        clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
        clpLaser = Sound.clipForLoopFactory("laser.wav");

    }

    // ===============================================
    // ==METHODS
    // ===============================================

    public static void main(String args[]) {
        EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
            public void run() {
                try {
                    Game game = new Game(); // construct itself
                    game.fireUpAnimThread();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void fireUpAnimThread() { // called initially
        if (thrAnim == null) {
            thrAnim = new Thread(this); // pass the thread a runnable object (this)
            thrAnim.start();
        }
    }

    // implements runnable - must have run method
    public void run() {

        // lower this thread's priority; let the "main" aka 'Event Dispatch'
        // thread do what it needs to do first
        thrAnim.setPriority(Thread.MIN_PRIORITY);

        // and get the current time
        long lStartTime = System.currentTimeMillis();

        // this thread animates the scene
        while (Thread.currentThread() == thrAnim) {
            tick();
            spawnNewShipFloater();
            gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must
            // surround the sleep() in a try/catch block
            // this simply controls delay time between
            // the frames of the animation

            //this might be a good place to check for collisions
            checkCollisions();
            //this might be a god place to check if the level is clear (no more foes)
            //if the level is clear then spawn some big asteroids -- the number of asteroids
            //should increase with the level.
            checkNewLevel();

            try {
                // The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update)
                // between frames takes longer than ANI_DELAY, then the difference between lStartTime -
                // System.currentTimeMillis() will be negative, then zero will be the sleep time
                lStartTime += ANI_DELAY;
                Thread.sleep(Math.max(0,
                        lStartTime - System.currentTimeMillis()));
            } catch (InterruptedException e) {
                // just skip this frame -- no big deal
                continue;
            }
        } // end while
    } // end run

    private void checkCollisions() {


        Point pntFriendCenter, pntFoeCenter;
        int nFriendRadiux, nFoeRadiux;

        for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
            for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

                pntFriendCenter = movFriend.getCenter();
                pntFoeCenter = movFoe.getCenter();
                nFriendRadiux = movFriend.getRadius();
                nFoeRadiux = movFoe.getRadius();

                //detect collision
                if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) {

                    //falcon
                    if ((movFriend instanceof Falcon) && !(movFriend instanceof FalconFriend)) {
                        if (!CommandCenter.getInstance().getFalcon().getProtected()) {
                            CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                            CommandCenter.getInstance().spawnFalcon(false);
                            CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore() - 1);
                        }
                    }
                    //not the falcon
                    else {
                        CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                    }//end else
                    //kill the foe and if asteroid, then spawn new asteroids
                    killFoe(movFoe);
                    Sound.playSound("kapow.wav");
                    CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore() + 1);

                }//end if
            }//end inner for
            if (movFriend instanceof FalconFriend && getTick() % 10 == 0){
                CommandCenter.getInstance().getOpsList().enqueue(new Bullet((FalconFriend) movFriend), CollisionOp.Operation.ADD);
                if (getTick()%1000==0){
                    CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                }
            }
        }//end outer for


        //check for collisions between falcon and floaters
        if (CommandCenter.getInstance().getFalcon() != null) {
            Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();
            int nFalRadiux = CommandCenter.getInstance().getFalcon().getRadius();
            Point pntFloaterCenter;
            int nFloaterRadiux;

            for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
                pntFloaterCenter = movFloater.getCenter();
                nFloaterRadiux = movFloater.getRadius();

                //detect collision
                if (pntFalCenter.distance(pntFloaterCenter) < (nFalRadiux + nFloaterRadiux)) {

                    CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
                    Sound.playSound("pacman_eatghost.wav");
                    CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons() * 2);
                }//end if
            }//end inner for
        }//end if not null


        //we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists while iterating them above
        while (!CommandCenter.getInstance().getOpsList().isEmpty()) {
            CollisionOp cop = CommandCenter.getInstance().getOpsList().dequeue();
            Movable mov = cop.getMovable();
            CollisionOp.Operation operation = cop.getOperation();

            switch (mov.getTeam()) {
                case FOE:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFoes().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFoes().remove(mov);
                    }

                    break;
                case FRIEND:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFriends().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFriends().remove(mov);
                    }
                    break;

                case FLOATER:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFloaters().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFloaters().remove(mov);
                    }
                    break;

                case DEBRIS:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovDebris().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovDebris().remove(mov);
                    }
                    break;


            }

        }
        //a request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
        System.gc();

    }//end method

    private void killFoe(Movable movFoe) {

        if (movFoe instanceof Asteroid) {

            //we know this is an Asteroid, so we can cast without threat of ClassCastException
            Asteroid astExploded = (Asteroid) movFoe;
            //big asteroid
            if (astExploded.getLives() < 0) {
                astExploded.takeLife();
            }
            else if (astExploded.getSize() == 0) {
                //spawn two medium Asteroids
                CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
                CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);

            }
            //medium size aseroid exploded
            else if (astExploded.getSize() == 1) {
                //spawn three small Asteroids
                CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
                CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
                CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);

            }

            if (astExploded.getColor().equals(Color.BLUE)) {
                CommandCenter.getInstance().getFalcon().setProtected(true);
            }
            if (astExploded.getColor().equals(Color.ORANGE)) {
                CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons() + 1);
            }
            if (astExploded.getColor().equals(Color.RED)) {
                for (int i = 0; i < Game.R.nextInt(10); i++) {
                    CommandCenter.getInstance().getOpsList().enqueue(new Cruise(), CollisionOp.Operation.ADD);
                }
            }
            if (astExploded.getColor().equals(Color.YELLOW)) {
                CommandCenter.getInstance().getOpsList().enqueue(new FalconFriend(CommandCenter.getInstance().getFalcon()), CollisionOp.Operation.ADD);
            }
            if (astExploded.getColor().equals(Color.GREEN)) {
                CommandCenter.getInstance().getFalcon().setCenter(new Point(
                        Game.R.nextInt(Game.DIM.width),
                        Game.R.nextInt(Game.DIM.height)
                ));
            }
            if (movFoe instanceof NewShipFloater) {
                CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons() / 2);
            }

            //remove the original Foe
            if (!(movFoe instanceof Asteroid && ((Asteroid) movFoe).getLives() < 0)) {
                CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
                CommandCenter.getInstance().getFalcon().setProtected(true);
            }

        }
    }

    //some methods for timing events in the game,
    //such as the appearance of UFOs, floaters (power-ups), etc.
    public void tick() {
        if (nTick == Integer.MAX_VALUE)
            nTick = 0;
        else
            nTick++;
    }

    public int getTick() {
        return nTick;
    }

    private void spawnNewShipFloater() {
        //make the appearance of power-up dependent upon ticks and levels
        //the higher the level the more frequent the appearance
        if (nTick % (SPAWN_NEW_SHIP_FLOATER - nLevel * 7) == 0) {
            //CommandCenter.getInstance().getMovFloaters().enqueue(new NewShipFloater());
            CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
        }
        if (nTick % (SPAWN_NEW_SHIP_FLOATER - nLevel * 10) == 0) {
            //CommandCenter.getInstance().getMovFloaters().enqueue(new NewShipFloater());
            CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(true), CollisionOp.Operation.ADD);
        }
    }

    // Called when user presses 's'
    private void startGame() {
        CommandCenter.getInstance().clearAll();
        CommandCenter.getInstance().initGame();
        CommandCenter.getInstance().setLevel(0);
        CommandCenter.getInstance().setPlaying(true);
        CommandCenter.getInstance().setPaused(false);
        //if (!bMuted)
        // clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
    }

    //this method spawns new asteroids
    private void spawnAsteroids(int nNum) {
        for (int nC = 0; nC < nNum; nC++) {
            //Asteroids with size of zero are big
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

            if (nNum % 5 ==0){
                int size = -1 * Game.R.nextInt(30);
                CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(size), CollisionOp.Operation.ADD);
            }
        }
    }

    private void spawnDebris(int nNum) {
        for (int nC = 0; nC < nNum; nC++) {
            //Asteroids with size of zero are big
            CommandCenter.getInstance().getOpsList().enqueue(new Debris(), CollisionOp.Operation.ADD);

        }
    }

    private boolean isLevelClear() {
        //if there are no more Asteroids on the screen
        boolean bAsteroidFree = true;
        for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
            if (movFoe instanceof Asteroid) {
                bAsteroidFree = false;
                break;
            }
        }

        return bAsteroidFree;


    }

    private void checkNewLevel() {

        if (isLevelClear()) {
            if (CommandCenter.getInstance().getFalcon() != null)
                CommandCenter.getInstance().getFalcon().setProtected(true);

            spawnAsteroids(CommandCenter.getInstance().getLevel() + 2);
            spawnDebris(CommandCenter.getInstance().getLevel()*3 + 5);
            CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);


        }
    }


    // Varargs for stopping looping-music-clips
    private static void stopLoopingSounds(Clip... clpClips) {
        for (Clip clp : clpClips) {
            clp.stop();
        }
    }

    // ===============================================
    // KEYLISTENER METHODS
    // ===============================================

    @Override
    public void keyPressed(KeyEvent e) {
        Falcon fal = CommandCenter.getInstance().getFalcon();
        int nKey = e.getKeyCode();
        // System.out.println(nKey);

        if (nKey == START && !CommandCenter.getInstance().isPlaying())
            startGame();

        if (fal != null) {

            switch (nKey) {
                case PAUSE:
                    CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
                    if (CommandCenter.getInstance().isPaused())
                        stopLoopingSounds(clpMusicBackground, clpThrust);
                    else
                        clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
                    break;
                case QUIT:
                    System.exit(0);
                    break;
                case UP:
                    fal.thrustOn();
                    if (!CommandCenter.getInstance().isPaused())
                        clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
                    break;
                case LEFT:

                    fal.rotateLeft();
                    break;
                case RIGHT:
                    fal.rotateRight();
                    break;
                case DOWN:
                    fal.setSlowed();
                    break;
                    // possible future use
                    // case KILL:
                    // case SHIELD:
                    // case NUM_ENTER:
                default:
                    break;

            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Falcon fal = CommandCenter.getInstance().getFalcon();
        int nKey = e.getKeyCode();
        System.out.println(nKey);

        if (fal != null) {
            switch (nKey) {
                case FIRE:
                    CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
                    Sound.playSound("laser.wav");
                    break;

                //special is a special weapon, current it just fires the cruise missile.
                case SPECIAL:
                    CommandCenter.getInstance().getOpsList().enqueue(new Cruise(fal), CollisionOp.Operation.ADD);
                    //Sound.playSound("laser.wav");
                    break;

                case LEFT:
                    fal.stopRotating();
                    break;
                case RIGHT:
                    fal.stopRotating();
                    break;
                case UP:
                    fal.thrustOff();
                    clpThrust.stop();
                    break;
                case CHARGESPECIAL:
                    try {
                        Thread.sleep(800);
                    }
                    catch (InterruptedException e1) {
                    }
                    for (int i = 0; i < Game.R.nextInt(8)+1;i++) {
                        CommandCenter.getInstance().getOpsList().enqueue(new Cruise(fal,true), CollisionOp.Operation.ADD);
                    }
                    break;
                case MUTE:
                    if (!bMuted) {
                        stopLoopingSounds(clpMusicBackground);
                        bMuted = !bMuted;
                    } else {
                        clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
                        bMuted = !bMuted;
                    }
                    break;


                default:
                    break;
            }
        }
    }

    @Override
    // Just need it b/c of KeyListener implementation
    public void keyTyped(KeyEvent e) {
    }

}


