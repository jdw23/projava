package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

public class FalconFriend extends Falcon{
    public FalconFriend(Falcon friend){
        super();
        setCenter(new Point(friend.getCenter().x+100, friend.getCenter().y));
        setDeltaX(10);
        setOrientation(-90);
    }

    @Override
    public void move() {
        super.move();
    }
}
