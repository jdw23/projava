package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Debris extends Sprite{
    public Debris(){

        //call Sprite constructor
        super();
        setTeam(Team.DEBRIS);
        //random delta-x
        int nDX = Game.R.nextInt(10);
        if(nDX %2 ==0)
            nDX = -nDX;
        setDeltaX(nDX);

        //random delta-y
        int nDY = Game.R.nextInt(10);
        if(nDY %2 ==0)
            nDY = -nDY;
        setDeltaY(nDY);

        ArrayList<Point> pntCs = new ArrayList<Point>();
        pntCs.add(new Point(0,0));
        pntCs.add(new Point(100,0));
        pntCs.add(new Point(0,100));
        pntCs.add(new Point(100,100));

        setRadius(30);
        assignPolarPoints(pntCs);

    }
    public void move(){
        super.move();
    }

    public void draw(Graphics g){
        super.draw(g);
    }

}
