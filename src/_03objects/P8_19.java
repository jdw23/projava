package _03objects;

import java.util.Scanner;

public class P8_19 {
    private double x;
    private double y;
    private double xVelocity;
    private double yVelocity;
    private final double G;
    public P8_19(double xIn){
        x=xIn;
        y=0;
        G= 9.8;
    }
    public void move(double sec){
        x=x+sec*xVelocity;
        y= yVelocity*sec -.5*G*Math.pow(sec,2);
        yVelocity = yVelocity-G*sec;
    }
    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public void shoot(double a, double v){
        xVelocity =  v*Math.cos(a);
        yVelocity = v*Math.sin(a);
        do {
            this.move(.1);
            System.out.println("X:"+this.getX()+", Y:"+this.getY());
        }while(this.getY() > 0);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input the angle then the velocity");
        double angle = scanner.nextDouble();
        double velocity = scanner.nextDouble();
        P8_19 cannon = new P8_19(0);
        cannon.shoot(angle, velocity);
    }
}
