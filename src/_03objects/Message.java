package _03objects;

public class Message{
    private String recipient;
    private String sender;
    private String message;

    public Message(String s, String r){
        recipient = r;
        sender = s;
        message = "";
    }
    public void append(String line){
        message = message+"\n"+line;
    }
    public String toString(){
        return "From: " + sender + "\n"
                + "To: " + recipient + message;
    }
}
