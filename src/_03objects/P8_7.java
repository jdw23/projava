package _03objects;

import java.util.Arrays;

public class P8_7 {
    private int[] combo;
    private boolean[] entered;
    private int current;
    private int counter;
    public P8_7(int s1, int s2, int s3){
        if (s1> 39 || s1<0 || s2> 39 || s2<0 || s3> 39 || s3<0){
            System.out.println("invalid lock");
        }
        else{
            combo = new int[]{s1,s2,s3};
            entered= new boolean[]{false,false,false};
            current = 0;
            counter=0;
        }
    }
    public void reset(){
        entered= new boolean[]{false,false,false};
        current = 0;
        counter =0;
    }
    public void turnRight(int ticks) {
        current=(current-ticks) % 40;
        if (current<0){
            current=40+current;
        }
        if (counter == 0 || counter == 2) {
            if (current == combo[counter]) {
                entered[counter] = true;
            }
        }
        counter++;
    }
    public void turnLeft(int ticks){
        current = (current + ticks) %40;
        if (counter==1){
            if (current==combo[counter]){
                entered[counter]=true;
            }
        }
        counter++;
    }
    public boolean open(){
        return counter==3 &&
                entered[0]==true &&
                entered[1]==true &&
                entered[2]==true;
    }
    //test
    public static void main(String[] args) {
        P8_7 lock = new P8_7(34,15,13);
        lock.turnRight(6);
        lock.turnLeft(2);
        lock.turnRight(2);
        System.out.println(lock.open());
        lock.reset();
        lock.turnRight(6);
        lock.turnLeft(21);
        lock.turnRight(2);
        System.out.println(lock.open());
    }
}
