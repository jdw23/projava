package _03objects;

public class P8_1 {
    private int time;
    private int level;
    public P8_1(){
        this.level = 1;
        this.time = 0;
    }
    public void add30(){
        this.time=this.time+30;
    }
    public void switchPowerLevel(){
        if (this.level==1){
            this.level =2;
        }
        else {
            this.level=1;
        }
    }
    public void reset(){
        this.level=1;
        this.time=0;
    }
    public void start(){
        System.out.println("Cooking for " + this.time +" seconds at level "+this.level);
    }

    public static void main(String[] args) {
        P8_1 microwave = new P8_1();
        microwave.add30();
        microwave.switchPowerLevel();
        microwave.reset();
        microwave.add30();
        microwave.add30();
        microwave.add30();
        microwave.switchPowerLevel();
        microwave.start();
    }
}
