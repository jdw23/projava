package _03objects;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class P8_14 {
    private String name;
    private int population;
    private double area;
    public P8_14(String n, int p,double a){
        name = n;
        population = p;
        area = a;
    }
    public int getPopulation(){
        return population;
    }

    public double getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    public double getDensity(){
        return ((float) population)/area;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter as many countries as you'd like. Name then population then area. One per line.");
        System.out.println("Example: Canada 10 10000");
        System.out.println("Type exit to end");
        ArrayList<P8_14> countries = new ArrayList<P8_14>();
        while(scanner.hasNext()){
            try {
                String lineInput = scanner.nextLine();
                if (lineInput.equals("exit")){

                    break;
                }
                String[] line = lineInput.split(" ");
                String name = line[0];
                int pop = Integer.parseInt(line[1]);
                double area = Double.parseDouble(line[2]);
                countries.add(new P8_14(name,pop,area));
            }
            catch (Exception e){
                System.out.println("Enter a valid format");
            }
        }
        int maxPopulation = 0;
        String maxPopulationName = "";
        double maxArea = 0;
        String maxAreaName = "";
        double maxDensity = 0;
        String maxDensityName = "";
        for (P8_14 country: countries){
            if (country.getPopulation() > maxPopulation){
                maxPopulation = country.getPopulation();
                maxPopulationName = country.getName();
            }
            if (country.getArea() > maxArea){
                maxArea = country.getArea();
                maxAreaName = country.getName();
            }
            if (country.getDensity() > maxDensity){
                maxDensity = country.getDensity();
                maxDensityName = country.getName();
            }
        }
        System.out.println("The max population is "+maxPopulationName);
        System.out.println("The max area is "+maxAreaName);
        System.out.println("The max density is " + maxDensityName);
    }
}
