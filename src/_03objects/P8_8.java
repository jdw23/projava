package _03objects;

public class P8_8 {
    private int demVotes;
    private int repVotes;
    public P8_8(){
        demVotes=0;
        repVotes=0;
    }
    public void voteDem(){
        demVotes++;
    }
    public void voteRepublican(){
        repVotes++;
    }
    public void resetElection(){
        demVotes=0;
        repVotes=0;
    }
    public void getTallies(){
        System.out.println("There are " + demVotes +" for Democrats");
        System.out.println("There are " + repVotes +" for Republicans");
    }

    public static void main(String[] args) {
        P8_8 voteMachine = new P8_8();
        for (int i = 0; i<400; i++){
            voteMachine.voteDem();
            voteMachine.voteRepublican();
        }
        voteMachine.resetElection();
        for (int i = 0; i<4000; i++){
            voteMachine.voteDem();
        }
        for (int i = 0; i<40; i++){
            voteMachine.voteRepublican();
        }
        voteMachine.getTallies();
    }
}
