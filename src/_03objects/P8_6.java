package _03objects;

public class P8_6 {
    private double fuel;
    private double mpg;
    public P8_6(int m){
        fuel = 0;
        mpg = m;
    }
    public void addGas(double g){
        this.fuel=this.fuel+g;
    }
    public void drive(double m){
        double maxDistance = fuel*mpg;
        if (maxDistance < m){
            System.out.println("uh oh! You ran out of gas after "+maxDistance+" miles");
            this.fuel=0;
        }
        else{
            this.fuel=this.fuel-(m/mpg);
        }
    }
    public double getGasLevel(){
        return this.fuel;
    }
    //test
    public static void main(String[] args) {
        P8_6 car = new P8_6(50);
        car.addGas(20);
        car.drive(100);
        System.out.println(car.getGasLevel());
        car.drive(100000);
    }
}
