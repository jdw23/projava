package _03objects;

public class P8_5 {
    private double radius;
    private double height;
    public P8_5(double r, double h){
        radius = r;
        height = h;
    }
    public double getSurfaceArea(){
        return (2*Math.PI*this.radius*this.height)+2*Math.PI*Math.pow(this.radius,2);
    }

    public double getVolume() {
        return Math.PI*Math.pow(this.radius,2)*this.height;
    }

    public static void main(String[] args) {
        P8_5 can = new P8_5(2,5.5);
        System.out.println("The surface area is" + can.getSurfaceArea());
        System.out.println("The volume is " + can.getVolume());
    }
}
