package _03objects;

import java.util.ArrayList;

public class P8_16 {
    ArrayList<Message> inbox;
    public P8_16(){
        inbox = new ArrayList<Message>();
    }
    public void addMessage(Message m){
        inbox.add(m);
    }
    public Message getMessage(int i){
        return inbox.get(i);
    }
    public void removeMessage(int i){
        inbox.remove(i);
    }

    public static void main(String[] args) {
        P8_16 mailbox = new P8_16();
        Message next = new Message("joe","joel");
        next.append("hi");
        mailbox.addMessage(next);
        System.out.println(mailbox.getMessage(0));
        mailbox.removeMessage(0);
    }
}
