package _07streams;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

//This is only returning "redder", but I'm assuming the question is prompting that rerunning this program will
//Have a different result every time, because parallelism does not gurantee order.
public class E19_14 {
    public static void main(String[] args) throws IOException {
        Stream<String> fileLines = Files.lines(Paths.get("test.txt"), Charset.defaultCharset()).parallel();
        Optional<String> ans = fileLines.flatMap(line -> Arrays.stream(line.split(" "))).filter((s)->s.length()>=5).filter((s)->
                (new StringBuilder(s).reverse().toString()).equals(s)).findAny();

        if (ans.isPresent()){
            System.out.println(ans.get());
        }


    }

}
