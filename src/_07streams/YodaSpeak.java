package _07streams;

import java.util.Scanner;

public class YodaSpeak {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a sentence");
        String input = scan.nextLine();
        String[] words =  input.split(" ");
        recursiveYoda(words, words.length-1);
        System.out.println("");
        iterateYoda(words);
    }
    public static void iterateYoda(String[] words){
        for(int i = words.length-1; i>=0;i--){
            System.out.print(words[i]+" ");
        }
    }
    public static void recursiveYoda(String[] words,int index){
        if (index < 0){
            return;
        }
        System.out.print(words[index]+" ");
        recursiveYoda(words, index-1);
    }
}
