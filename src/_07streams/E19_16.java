package _07streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class E19_16 {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File file = new File("text.txt");
        Scanner scanner = new Scanner(file);
        String input = "";
        while (scanner.hasNextLine()){
            input=input+scanner.nextLine();
        }
        String[] toConvert = input.split(" ");
        Arrays.stream(toConvert).map(String::toLowerCase).collect(Collectors.groupingBy(
                (s)-> {return s.substring(0,1);}, Collectors.averagingDouble((s)->s.length())
        )).forEach((s,d) -> System.out.println(s +" "+d));
    }


}
