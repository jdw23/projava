package _07streams;


import java.util.Currency;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_6 {
    public static void main(String[] args) {
        Set<Currency> input = Currency.getAvailableCurrencies();
        String output = input.stream().map(Currency::toString).collect(Collectors.joining("\n"));
        System.out.println(output);
    }
}
