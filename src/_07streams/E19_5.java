package _07streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_5 {
    public static void main(String[] args) {
        List<String> alpha = Arrays.asList("a", "b", "c", "d");
        System.out.println(toString(alpha.stream(),10));
    }
    public static <T> String toString(Stream<T> stream, int n){
        return stream.limit(n).map(T::toString).collect(Collectors.joining(","));
    }
}
