package _07streams;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class E13_20 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a dollar amount");
        int input = scanner.nextInt();
        int[] path = new int[] {0,0,0,0};
        ArrayList<Integer[]> paths = new ArrayList<Integer[]>();
        change(input, path,paths);
    }
    public static void change(int amount,int[] curPath, ArrayList<Integer[]> paths){
        if (amount <0){
            return;
        }
        if (amount==0){
            Integer[] boxedPath = Arrays.stream(curPath).boxed().toArray(Integer[]::new);
            for (Integer[] path: paths) {
                if (Arrays.equals(path,boxedPath)) {
                    return;
                }
            }
            String output = String.format("100s: %d 20s: %d 5s: %d 1s: %d",curPath[0],curPath[1],curPath[2],curPath[3]);
            System.out.println(output);
            paths.add(boxedPath);
            return;
        }
        if (amount >= 100){
            curPath[0]++;
            change(amount-100,curPath,paths);
            curPath[0]--;
        }
        if (amount >= 20 ){
            curPath[1]++;
            change(amount-20,curPath,paths);
            curPath[1]--;
        }
        if (amount >= 5){
            curPath[2]++;
            change(amount-5,curPath,paths);
            curPath[2]--;
        }
        if (amount >= 1){
            curPath[3]++;
            change(amount-1,curPath,paths);
            curPath[3]--;
        }

    }
}
