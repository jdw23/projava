package _07streams;

import java.util.Scanner;

public class P13_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int input = scanner.nextInt();
        if (input < 0){
            return;
        }
        String in = Integer.toString(input);
        generateNums(in,"");
    }
    public static void generateNums(String input,String path){
        String[] decode = new String[]{"","","ABC","DEF","GHI","JKL","MNO",
                "PQRS","TUV","WXYZ"};
        if (input.equals("")) {
            System.out.println(path);
        }
        else {
            int num = Integer.parseInt(input.substring(0,1));
            for (char c : decode[num].toCharArray()){
                generateNums(input.substring(1),path.concat(Character.toString(c)));
            }
            if (num==0 || num == 1){
                generateNums(input.substring(1),path);
            }
        }
    }

}
