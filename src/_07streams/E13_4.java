package _07streams;

import java.util.Scanner;

public class E13_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input integer");
        int in = scanner.nextInt();
        System.out.println(findBinaryString(in));
    }
    public static String findBinaryString(int d){
        return Integer.toString(findBinary(d));
    }
    public static int findBinary(int decimal){
        if (decimal == 0){
            return 0;
        }
        else
            return decimal % 2 + 10 * (findBinary(decimal / 2));
    }

}
