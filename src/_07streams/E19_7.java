package _07streams;

import sun.misc.JavaObjectInputStreamReadString;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public class E19_7 {
    public static void main(String[] args) {

        Function<String, String> method = (String s) -> {
            return s.substring(0, 1) + "..." + s.substring(s.length() - 1);
        };
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter words");
        String input = scan.nextLine();
        String[] toConvert = input.split(" ");
        Arrays.stream(toConvert).filter((s)-> s.length()>=2).map(method).forEach((y) -> System.out.println(y));
    }
}
