package _01control;

import java.util.Scanner;

public class E2_4 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int num1 = scan.nextInt();
        int num2 = scan.nextInt();

        System.out.println(num1+num2);
        System.out.println(num1-num2);
        System.out.println(num1*num2);
        System.out.println((num1+num2)/2);
        System.out.println(Math.abs(num1-num2));
        System.out.println(Math.max(num1,num2));
        System.out.println(Math.min(num1,num2));
    }
}
