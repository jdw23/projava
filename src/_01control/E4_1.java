package _01control;

import java.util.Scanner;

public class E4_1 {
    public static void main(String[] args){
        int sumEven = 0;
        int sumSquares = 0;
        int sumPowers = 0;
        for (int i =1; i <101;i++){
            if (i % 2==0){
                sumEven=sumEven+i;
            }
            sumSquares= (int) (sumSquares+Math.pow(i,2));
        }
        for (int i=0;i<21;i++){
            sumPowers+=Math.pow(2,i);
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter two integers");
        int a = scan.nextInt();
        int b = scan.nextInt();
        int sumOdds = 0;
        for (int i=a; i <= b;i++){
            if (i % 2 == 1) {
                sumOdds += i;
            }
        }
        System.out.println("Enter 1 integer");
        int input = scan.nextInt();
        int oddDigitTotal = 0;
        while (input > 0){
            String inputString = Integer.toString(input);
            int digit = Integer.parseInt(inputString.substring(0, 1));
            input = (int) (input % (Math.pow(10,inputString.length()-1)));
            if (digit % 2==1) {
                oddDigitTotal = oddDigitTotal + digit;
            }
        }
        System.out.println(sumEven);
        System.out.println(sumSquares);
        System.out.println(sumPowers);
        System.out.println(sumOdds);
        System.out.println(oddDigitTotal);

    }
}
