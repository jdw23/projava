package _01control;

import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Month?");
        int month = scan.nextInt();
        System.out.println("Day?");
        int day = scan.nextInt();
        String season="";
        if (month >0 && month <4){
            season = "Winter";
        }
        else if (month < 7){
            season = "Spring";
        }
        else if (month < 10){
            season = "Summer";
        }
        else if (month < 13){
            season = "Fall";
        }
        if (month % 3==0 && day >=21){
            if (season=="Winter"){
                season="Spring";
            }
            else if (season=="Spring"){
                season="Summer";
            }
            else if (season=="Summer"){
                season="Fall";
            }
            else {
                season="Winter";
            }
        }
        System.out.println(season);
    }
}
