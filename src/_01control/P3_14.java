package _01control;

import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int year = scan.nextInt();
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 !=0)){
            System.out.println("Leap year");
        }
        else {
            System.out.println("Not a leap year");
        }
    }
}
