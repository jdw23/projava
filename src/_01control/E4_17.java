package _01control;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter an integer");
        int number = scanner.nextInt();
        while (number>0){
            System.out.println(number % 2);
            number = number/2;
        }
    }
}
