package _01control;

import java.util.Collections;

public class E2_20 {
    public static void main(String[] args){
        //copy and pasted from book, change to change size of base
        int baseLength = "/      \\".length();
        //height of the tree trunk
        int treeTrunkHeight = 2;
        for (int i = 0; i<baseLength; i+=2) {
            int spacesBufferLength = (baseLength - i - 2) / 2;
            String spacesBufferOutside = String.join("", Collections.nCopies(spacesBufferLength," "));
            String spacesBufferInside = String.join("", Collections.nCopies(i," "));
            System.out.println(spacesBufferOutside+"/"+spacesBufferInside+"\\"+spacesBufferOutside);
        }
        System.out.println(String.join("", Collections.nCopies(baseLength,"-")));

        for (int b = 0; b<treeTrunkHeight;b++){
            String midSection = "\"  \"";
            int spacesBufferLength = (baseLength - midSection.length()) / 2;
            String spacesBufferOutside = String.join("", Collections.nCopies(spacesBufferLength," "));
            System.out.println(spacesBufferOutside+midSection+spacesBufferOutside);
        }

    }
}
