package _01control;

import java.util.Scanner;

public class E2_6 {
    public static void main(String[] args) {
        double meterToMile = 0.00062137;
        double meterToFeet = 3.28084;
        double meterToInch = 39.3701;
        Scanner scan = new Scanner(System.in);
        System.out.println("Input a measurement in meters:");
        double meter = scan.nextDouble();
        System.out.println((meter*meterToMile)+" Mile(s)");
        System.out.println((meter*meterToFeet)+" Feet");
        System.out.println((meter*meterToInch)+" Inch(es)");
    }
}
