package _01control;

import java.util.Scanner;

public class P3_7 {
    public static void main(String[] args){
        System.out.println("Input salary");
        Scanner scan = new Scanner(System.in);
        double salary = scan.nextDouble();
        double taxes = 0.0;
        taxes = taxes + .01*Math.min(salary, 50000);
        if (salary > 50000){
            taxes = taxes + .02*(Math.min(salary,75000)-50000);
        }
        if (salary > 75000){
            taxes = taxes + .03*(Math.min(salary,100000)-75000);
        }
        if (salary > 100000){
            taxes = taxes + .04*(Math.min(salary,250000)-100000);
        }
        if (salary > 250000){
            taxes = taxes + .05*(Math.min(salary,500000)-250000);
        }
        if (salary > 500000){
            taxes = taxes + .06*(salary-500000);
        }
        System.out.println(taxes);
    }
}
