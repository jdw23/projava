package _01control;

import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args){
        String numeral = "";
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt();
        while (input >= 1000){
            numeral=numeral+"M";
            input = input-1000;
        }
        while (input >= 900){
            numeral=numeral+"CM";
            input = input -900;
        }
        while (input >= 500){
            numeral=numeral+"D";
            input = input-500;
        }
        while (input >= 400){
            numeral=numeral+"CD";
            input=input-400;
        }
        while (input >= 100){
            numeral=numeral+"C";
            input=input-100;
        }
        while (input >= 90){
            numeral=numeral+"XC";
            input=input-90;
        }
        while (input >= 50){
            numeral = numeral+"L";
            input = input-50;
        }
        while (input >= 40){
            numeral = numeral+"XL";
            input = input-40;
        }
        while (input >= 10){
            numeral = numeral+"X";
            input = input-10;
        }
        while (input>=9){
            numeral = numeral+"IX";
            input = input-9;
        }
        while (input>=5){
            numeral = numeral+"V";
            input=input-5;
        }
        while (input>=4){
            numeral = numeral+"IV";
            input = input-4;
        }
        while (input>=1){
            numeral=numeral+"I";
            input=input-1;
        }
    System.out.println(numeral);
    }
}
