package _01control;
import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        float input = scan.nextFloat();
        int dollars = (int) input;
        int cents = (int) (100*(input-dollars)+0.5);
        System.out.println(dollars+" Dollar(s) and "+cents+" cent(s)");
    }
}

    //Assign the price to an integer variable dollars.
    // Multiply the difference price - dollars by 100 and add 0.5.
    // Assign the result to an integer variable cents.