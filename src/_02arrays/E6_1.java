package _02arrays;
import java.util.Random;

public class E6_1 {
    public static void main(String[] args) {
        final int SIZE = 10;
        int[] rands = new int[SIZE];
        Random rand = new Random();
        for (int i = 0; i<SIZE;i++){
            rands[i]= rand.nextInt();
        }
        String evenIndex = "";
        for(int i =0; i<SIZE;i=i+2 ){
            evenIndex = evenIndex + rands[i]+" ";
        }
        System.out.println(evenIndex);

        String evenValue="";
        for (int i = 0; i<SIZE;i++){
            if (rands[i] % 2==0){
                evenValue = evenValue +rands[i]+" ";
            }
        }
        System.out.println(evenValue);

        String reverse="";
        for (int i = SIZE-1;i>=0;i--){
            reverse=reverse+rands[i]+" ";
        }
        System.out.println(reverse);

        System.out.println(rands[0]+" "+rands[SIZE-1]);
    }
}
