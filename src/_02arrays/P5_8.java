package _02arrays;

public class P5_8 {
    /*
    public static void main(String[] args) {
        System.out.println(isLeapYear(2000));
        System.out.println(isLeapYear(1996));
        System.out.println(isLeapYear(1900));
        System.out.println(isLeapYear(1901));
    }
     */
    public static boolean isLeapYear(int year){
        if (year %400==0){
            return true;
        }
        if (year % 100 !=0 && year %4==0){
            return true;
        }
        return false;
    }
}
