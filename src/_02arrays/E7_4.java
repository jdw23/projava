package _02arrays;
import java.io.*;
import java.util.Scanner;

public class E7_4 {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Give input file name:");

        Scanner sysScan = new Scanner(System.in);
        String fileName = sysScan.nextLine();
        File text = new File(fileName);

        System.out.println("Give output file name:");
        String outputName = sysScan.nextLine();

        FileWriter fileWriter = new FileWriter(outputName);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        Scanner scan = new Scanner(text);
        int lineNum=1;

        while(scan.hasNextLine()){
            String line = scan.nextLine();
            printWriter.printf("/* %o */ %s\n",lineNum,line);
            lineNum++;
        }
        printWriter.close();
    }

}
