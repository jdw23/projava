package _02arrays;
import java.util.Random;
import java.util.Arrays;

public class E6_12 {
    public static void main(String[] args) {
        final int SIZE = 20;
        int[] a = new int[SIZE];
        Random rand = new Random();
        for (int i=0; i<SIZE;i++){
            a[i] = rand.nextInt(100);
        }
        System.out.println(Arrays.toString(a));
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));
    }
}
