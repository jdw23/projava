package _02arrays;

public class P5_24 {
    public static void main(String[] args) {
        System.out.println(rnToInt("MCMLXXVIII"));
    }
    public static int mapRNToInt(char rn){
        switch (rn){
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D': return 500;
            case 'M': return 1000;
        }
        return -1;
    }
    public static int rnToInt(String s){
        int total = 0;
        String rnStrLeft = s;

        while (rnStrLeft.length()!=0){
            if (rnStrLeft.length()==1 || mapRNToInt(rnStrLeft.charAt(0)) >= mapRNToInt(rnStrLeft.charAt(1))){
                total+=mapRNToInt(rnStrLeft.charAt(0));
                rnStrLeft = rnStrLeft.substring(1);
            }
            else {
                int diff = mapRNToInt(rnStrLeft.charAt(1))-mapRNToInt(rnStrLeft.charAt(0));
                total+=diff;
                rnStrLeft = rnStrLeft.substring(2);
            }
        }
        return total;
    }
}
