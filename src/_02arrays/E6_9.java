package _02arrays;

public class E6_9 {
    /*
    public static void main(String[] args) {
        int[] a = new int[]{1,2,3,4,5};
        int[] b = new int[]{1,2,3,4,5};
        int[] c = new int[]{1,2,3};
        int[] d = new int[]{2,3,4};
        System.out.println(equals(a,b));
        System.out.println(equals(c,d));
        System.out.println(equals(a,c));
    }
    */
    public static boolean equals(int[] a, int[] b){
        if (a.length != b.length){
            return false;
        }
        for (int i =0; i<a.length;i++){
            if (a[i]!=b[i]){
                return false;
            }
        }
        return true;
    }
}
