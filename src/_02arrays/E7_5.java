package _02arrays;

import java.io.*;
import java.util.Scanner;

public class E7_5 {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Give input file name:");

        Scanner sysScan = new Scanner(System.in);
        String fileName = sysScan.nextLine();
        File text = new File(fileName);
        Scanner scan;
        try {
            scan = new Scanner(text);
        }
        catch (Exception e){
            System.out.println("Please give an input file name :(");
            fileName = sysScan.nextLine();
            text = new File(fileName);
            scan = new Scanner(text);
        }

        System.out.println("Give output file name:");
        String outputName = sysScan.nextLine();
        FileWriter fileWriter;

        try {
            fileWriter = new FileWriter(outputName);
        }
        catch (Exception e){
            System.out.println("Please give an output file name :(");
            outputName = sysScan.nextLine();
            fileWriter = new FileWriter(outputName);
        }

        PrintWriter printWriter = new PrintWriter(fileWriter);

        int lineNum=1;

        while(scan.hasNextLine()){
            String line = scan.nextLine();
            printWriter.printf("/* %o */ %s\n",lineNum,line);
            lineNum++;
        }
        printWriter.close();
    }
}
