package _02arrays;
import java.util.Scanner;
import java.util.Arrays;

public class E6_16 {
    public static void main(String[] args) {
        //Read in numbers from a line
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        String[] num_input = input.split(" ");

        //Convert Strings to ints
        int maxInt = 0;
        int[] nums = new int[num_input.length];
        for (int i =0; i<nums.length;i++){
            nums[i] = Integer.parseInt(num_input[i]);
            if (nums[i]>maxInt){
                maxInt=nums[i];
            }
        }
        System.out.println("hi");
        //Normalize to max 20
        final int TALLEST = 20;
        for (int i=0; i<nums.length;i++){
            nums[i]= (int) (20* ( ((float) nums[i])/ ((float) maxInt)));
        }

        //Build and print each line
        for (int i= TALLEST; i >= 0;i--){
            String line="";
            for (int index=0;index<nums.length;index++){
                if (nums[index]>=i){
                    line+="*";
                }
                else{
                    line+=" ";
                }
            }
            System.out.println(line);
            line="";
        }
    }
}
