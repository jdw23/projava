#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################
R12.1 OO Analysis and Design

1.	Discover classes
2.	Determine the jobs of each class
3.	Determine the relationships between each class

R12.2 OO Analysis and Design

Classes are the nouns when describing a project.

R12.3 OO Analysis and Design

Methods are the verbs when describing a project.

R12.8 Relationships

Coin
---
getValue --- returns the value of the coin
getName  --- returns name of the coin

Cash Register
---
addItem      --- records the sale of an item
enterPayment --- takes coins and counts them towards the payment
giveChange   --- computes change and resets the machine for the next transaction

R12.9 CRC Quiz

Question
---
setText     --- sets the text of the question
setAnswer   --- stets the correct answer for the question
checkAnswer --- checks if an answer is correct
display     --- shows the question

Quiz
---
addQuestion      --- puts a question in the quiz
presentQuestions --- displays all the questions

R12.10 UML Quiz

R12_10.uml

R12.11 CRC Cards

The country with the largest area.
			•The country with the largest population.
			•The country with the largest population density (people per square kilometer).
Country
---
setArea --- sets the area
setPopulation --- sets the population size
getArea --- gets the area
getPopulation --- gets the population size
getPopulationDensity --- gets the population density

Psuedocode in Country and CountryDriver

R12.13 UML Diagram

Vending Machine, vending machine will store products and use coins to pay, similar to the cash register.

Coins, used to pay for products

Product, stored in the vending machine

R12.14 UML Diagram

Employee

Paycheck

R12.15 ULM Diagram

Customers order products from a store. Invoices are generated to list the items and quantities ordered,
payments received, and amounts still due. Products are shipped to the shipping address of the customer,
and invoices are sent to the billing address.

Draw a UML diagram showing the aggregation relationships between the classes Invoice, Address, Customer, and Product.
