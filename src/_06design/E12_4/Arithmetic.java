package _06design.E12_4;

import java.util.Random;
import java.util.Scanner;

public class Arithmetic {
    public static void main(String[] args) {
        System.out.println("Select a level:1,2,3");
        Scanner scanner = new Scanner(System.in);
        int level=0;
        while (level > 3  || level <1){
            level = scanner.nextInt();
        }
        if (level ==1){
            int[] ans = generateCombo(true, true);
            System.out.println("What is "+ans[0]+"+"+ans[1]);
            if (scanner.nextInt()==ans[0]+ans[1]){
                System.out.println("True!");
            }
            else{
                System.out.println("False");
            }
        }
        if (level ==2){
            int[] ans = generateCombo(false, true);
            System.out.println("What is "+ans[0]+"+"+ans[1]);
            if (scanner.nextInt()==ans[0]+ans[1]){
                System.out.println("True!");
            }
            else{
                System.out.println("False");
            }
        }
        if (level==3){
            int[] ans = generateCombo(true, true);
            System.out.println("What is "+ans[0]+"-"+ans[1]);
            if (scanner.nextInt()==ans[0]-ans[1]){
                System.out.println("True!");
            }
            else{
                System.out.println("False");
            }

        }
    }
    public static int[] generateCombo(boolean capped, boolean add){
        Random rand = new Random();
        int first = rand.nextInt(9)+1;
        int second;
        if (capped){
            second = rand.nextInt(10-first)+1;
            int[] ret = new int[]{first,second};
            return ret;
        }
        if (!capped && add){
            second = rand.nextInt(9)+1;
            int[] ret = new int[]{first,second};
            return ret;
        }
        else{
            second = rand.nextInt(first)+1;
            int[] ret = new int[]{first,second};
            return ret;
        }
    }
}
