package _06design.P12_1;

import java.util.ArrayList;

public class VendingMachine {

    //Vending Machine
    //Add product, remove product, take coins purchase, stores coins
    //Products
    //Have name, price
    //Coins
    //represents money
    private ArrayList<Product> products;
    private double bufferedCost;
    private double changeAvailable;

    public VendingMachine(){
        products = new ArrayList<Product>();
        changeAvailable=0;
        bufferedCost=0;
    }

    public void addCoin(Coin c){
        this.bufferedCost = this.bufferedCost+c.getValue();
    }
    public void addProduct(Product p){
        products.add(p);
    }
    public boolean buyProduct(int i){
        Product product;
        try {
            product = products.get(i-1);
        }
        catch (Exception e) {
            return false;
        }
        if (bufferedCost >= product.getPrice()){
            changeAvailable=changeAvailable+bufferedCost;
            bufferedCost = 0;
            products.remove(i-1);
            return true;
        }
        bufferedCost=0;
        return false;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public double getBufferedCost() {
        return bufferedCost;
    }

    public double getChangeAvailable() {
        return changeAvailable;
    }
}
