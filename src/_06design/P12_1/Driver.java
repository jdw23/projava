package _06design.P12_1;

import java.util.ArrayList;
import java.util.Scanner;

public class Driver {
    //Write a program that simulates a vending machine. Products can be purchased by inserting coins with a value at least equal to the cost of the product.
    // A user selects a product from a list of available products, adds coins, and either gets the product or gets the coins returned.
    // The coins are returned if insufficient money was supplied or if the product is sold out. The machine does not give change if too much money was added.
    // Products can be restocked and money removed by an operator.
    // Follow the design process that was described in this chapter. Your solution should include a class VendingMachine that is not coupled with the Scanner or PrintStream classes.


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to the vending machine!");
        int input=-1;
        VendingMachine vend = new VendingMachine();
        while (input!=6){
            System.out.println("1: put a coin in");
            System.out.println("2: see the products");
            System.out.println("3: buy a product");
            System.out.println("4: add a product to the machine");
            System.out.println("5: Check the money in the machine");
            System.out.println("6: exit");
            input = scanner.nextInt();
            switch (input){
                case 1:
                    System.out.println("How much is this coin?");
                    vend.addCoin(new Coin(scanner.nextDouble()));
                    break;
                case 2:
                    System.out.println(formatVend(vend.getProducts()));
                    break;
                case 3:
                    System.out.println("Select a number:");
                    System.out.println(formatVend(vend.getProducts()));
                    int productNum = scanner.nextInt();
                    boolean success = vend.buyProduct(productNum);
                    if (success){
                        System.out.println("You bought "+productNum);
                    }
                    else {
                        System.out.println("Buy failed :( change returned");
                    }
                    break;
                case 4:
                    System.out.println("Product name?");
                    scanner.skip("\\R");
                    String name = scanner.nextLine();
                    System.out.println("Price?");
                    double price = scanner.nextDouble();
                    vend.addProduct(new Product(name,price));
                    break;
                case 5:
                    System.out.println(vend.getBufferedCost()+" added by you");
                    System.out.println(vend.getChangeAvailable()+" in the machine");
            }
        }

    }
    public static String formatVend(ArrayList<Product> products){
        int prodNum = 1;
        String ret = "";
        for (Product p : products){
            ret=ret +Integer.toString(prodNum)+": "+p.getName()+", "+Double.toString(p.getPrice())+"\n";
            prodNum++;
        }
        return ret;
    }
}
