package _06design.P12_1;

public class Coin {
    private double value;
    public Coin(double value){
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
