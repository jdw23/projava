package _06design.P12_1;

import java.io.PrintWriter;

public class Product {
    private String name;
    private double price;
    public Product(String name, double price){
        this.name=name;
        this.price=price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
